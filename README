pcb2blender is a tool to convert PCB files to 3D Blender models.

 For the moment pcb2blender only supports Cadsoft Eagle. It's coded in
Python, and requires Eagle (at least v6) and Blender (at least 2.66)
to be installed. Also it needs imagemagick for creating
textures. pcb2blender is tested on Ubuntu Linux but it should work on
other systems too (such as Windows).

Usage:

>> blender -P eagle2blender.py -- <eagle-board-file>

Example:

>> blender -P eagle2blender.py -- boardfile.brd

Note: You should first download the models from
https://bitbucket.org/hyOzd/pcb2blender-models/downloads. Place models
under this directory in a folder named "models". Or use `--library`
option, see below.

What this script does:
- Board outline and drills
- Textures
- Place component models

Command line options:

Note that all command line options for pcb2blender must be added
after '--' and before file name.

--no-textures : disables texture generation, will still use existing
			    textures though. Useful since texture generation takes
			    longest time

example: blender -P eagle2blender.py -- --no-textures boardfile.brd

--dpi X : set texture DPI to X. X must be an integer.
          Use lower values (such as 120) for faster operation,
		  higher values (such as 720) for higher quality.

--library path : path to models library
                 By default script will look for models under the ./models
				 directory.

example: blender -P eagle2blender.py -- --dpi 120 boardfile.brd
