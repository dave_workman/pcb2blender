# pcb2blender model library related functions
import xml.etree.ElementTree as et
from math import radians

libRoot = None
mPath = ''

"""Read index.xml file from ./models folder"""
def initLibrary(modelsPath):

    global libRoot
    global mPath

    tree = et.parse(modelsPath+'index.xml')
    libRoot = tree.getroot()

    mPath = modelsPath

    return libRoot

"""Result is a list of tuples in format of:"""
"""(filename,modelname,xoffs,yoffs,scale,rotation[radians])"""
def findModel(tool,libName,pacName):

    global libRoot

    findings = list()

    # find blender models
    blend_nodes = libRoot.findall("file")

    for file in blend_nodes:
        for model in file:
            for package in model:
                if package.attrib['tool']==tool and \
                   package.attrib['library']==libName and \
                   package.attrib['name']==pacName :
                    fname = mPath+file.attrib['name']
                    mname = model.attrib['name']
                    xoffs = float(package.get('xoffs',0))
                    yoffs = float(package.get('yoffs',0))
                    scale = float(package.get('scale',1))
                    rot = radians(float(package.get('rot',0)))
                    findings.append((fname,mname,xoffs,yoffs,scale,rot))

    # find STL models
    stl_nodes = libRoot.findall("stl")

    for model in stl_nodes:
        for package in model:
            if package.attrib['tool']==tool and \
               package.attrib['library']==libName and \
               package.attrib['name']==pacName :
                fname = mPath + model.attrib['name']
                mname = model.attrib['name']
                # TODO: remove code duplication
                xoffs = float(package.get('xoffs',0))
                yoffs = float(package.get('yoffs',0))
                scale = float(package.get('scale',1))
                rot = radians(float(package.get('rot',0)))
                findings.append((fname,mname,xoffs,yoffs,scale,rot))

    if len(findings)==0:
        return None
    else:
        return findings
