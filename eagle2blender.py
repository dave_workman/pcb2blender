# fix blender not importing modules from working dir
import sys
import os
scriptpath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(scriptpath)

import xml.etree.ElementTree as et
from utils import *
from eagleutils import *
import blenderutils as bu
import libraryutils as lu
import math
import argparse

def init_argparser():
    """Initializes and returns command line argument parser."""
    parser = argparse.ArgumentParser(
        description="Creates a 3D blender file from eagle board file.")
    parser.add_argument('--no-textures', action='store_true',
                        help='disables texture generation')
    parser.add_argument('--dpi', type=int, default=720,
                        help='texture DPI value')
    parser.add_argument('--library', default=None,
                        help='models library directory, default: ./models')
    parser.add_argument('filename')
    return parser

def run():
    # handle command line arguments
    parser = init_argparser()
    i = sys.argv.index('--')
    args = parser.parse_args(sys.argv[i+1:])

    # set model library path, and scan index.xml
    if not args.library:
        modelsPath = scriptpath+'/models/'
    else:
        modelsPath = os.path.abspath(args.library) + "/"

    file = os.path.abspath(args.filename)
    # change working dir
    os.chdir(os.path.dirname(file))

    lu.initLibrary(modelsPath)

    # Read Eagle (XML) file
    tree = et.parse(file)
    root = tree.getroot()

    # find board outline
    dimwires = root.findall(".//plain/wire[@layer='20']")
    lines = getWiresInPath(getWireListFromET(dimwires))
    path = getPointsFromPath(lines)

    # find drill holes
    dholes = root.findall(".//plain/hole")
    dholes += root.findall(".//signal/via")

    holes = [Hole(e.attrib['x'],
                  e.attrib['y'],
                  e.attrib['drill']) for e in dholes]

    # find board elements
    elements = root.findall(".//board/elements/element")

    # list of models to be placed
    models = list()

    # for each component (element) on the board
    for el in elements:
        libName = el.attrib['library']
        pacName = el.attrib['package']

        # rotation and mirrored info is stored as [M]RXX, for ex: R90,MR90
        rot = el.get('rot')
        if rot == None:
            rot = 0
            mirrored = False
        else:
            if rot[0]=='M':
                mirrored = True
                rot = rot[1:]
            else:
                mirrored = False
            rot = math.radians(float(rot[1:]))

        # select all pads and holes in the component (they will be a part of board model)
        # everything with a 'drill' attribute
        nholes = root.findall(".//library[@name='"+libName+"']/" \
                            "packages/package[@name='"+pacName+"']/*[@drill]")

        for n in nholes:
            h = Hole(float(n.attrib['x']),n.attrib['y'],n.attrib['drill'])
            # map element coordinates to board coordinates
            h.pos = h.pos.rotated(rot)
            if mirrored: h.pos.x = -(h.pos.x)
            h.pos += Point(el.attrib['x'],el.attrib['y'])
            holes.append(h)

        results = lu.findModel('eagle',libName,pacName)
        if results != None:
            # select first option
            model = results[0]
            elx = float(el.attrib['x'])+model[2]
            ely = float(el.attrib['y'])+model[3]

            models.append((el.attrib['name'],
                           model[0],
                           model[1],
                           model[2]+elx,
                           model[3]+ely,
                           model[4],
                           model[5]+rot,
                           mirrored))

    bu.clearScene()
    bu.drawBoard(path)
    if not args.no_textures:
        makeTextures(file, args.dpi)
    bu.applyTextures()
    bu.extractHoles(holes)

    for m in models:
        bu.placeModel(*m)

if __name__ == "__main__":
    run()
